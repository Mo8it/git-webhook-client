FROM docker.io/library/rust:latest AS chef
RUN cargo install cargo-chef
WORKDIR app

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef AS builder
RUN apt update && \
	apt install libsqlite3-dev -y
COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json
COPY . .
RUN cargo build --release --bin git-webhook-client

FROM docker.io/library/debian:stable-slim AS runtime
WORKDIR app
RUN apt update && \
	apt install libsqlite3-dev -y
COPY --from=builder /app/target/release/git-webhook-client /usr/local/bin/git-webhook-client
CMD ["git-webhook-client"]