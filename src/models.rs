use diesel::prelude::{Insertable, Queryable};
use serde::Serialize;

use crate::schema::hooklog;

#[derive(Queryable, Serialize)]
pub struct HookLog {
    pub id: i32,
    pub datetime: String,
    pub clone_url: String,
    pub command_with_args: String,
    pub current_dir: String,
    pub stdout: Option<String>,
    pub stderr: Option<String>,
    pub status_code: Option<i32>,
}

#[derive(Insertable)]
#[diesel(table_name = hooklog)]
pub struct NewHookLog<'a> {
    pub datetime: &'a str,
    pub clone_url: &'a str,
    pub command_with_args: &'a str,
    pub current_dir: &'a str,
}
