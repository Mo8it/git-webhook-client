use anyhow::Context;
use askama_axum::IntoResponse;
use axum::{
    extract::{Query, State},
    response::Response,
};
use serde::Deserialize;
use std::sync::Arc;
use tokio::task;
use tracing::info;

use crate::{
    db, errors::AppError, extractors::ValidatedJson, mailer::Mailer, states, templates, webhook,
};

#[derive(Deserialize)]
pub struct IndexQuery {
    id: Option<i32>,
}

pub async fn index(
    State(db): State<Arc<states::DB>>,
    query: Query<IndexQuery>,
) -> Result<Response, AppError> {
    let id = match query.id {
        Some(id) if id != 0 => id,
        Some(_) => return Err("id=0 not allowed!".into()),
        None => -1,
    };

    let hook_log = db::get_hook_log(&db.pool, id)?;

    info!("Viewed hook log with id: {}", hook_log.id);

    let template = templates::HookLog::from(hook_log);

    Ok(template.into_response())
}

pub async fn trigger(
    State(db): State<Arc<states::DB>>,
    State(state_config): State<Arc<states::StateConfig>>,
    State(mailer): State<Arc<Mailer>>,
    ValidatedJson(json): ValidatedJson,
) -> Result<Response, AppError> {
    info!("Trigger called");

    let repo = json
        .get("repository")
        .context("Can not get the repository value from the request body!")?;

    let clone_url = repo
        .get("clone_url")
        .context("Can not get value clone_url from repository in the request body!")?;

    let clone_url = clone_url
        .as_str()
        .context("The value of clone_url from repository in the request body is not a string!")?;

    let hook = state_config.get_hook(clone_url).with_context(|| {
        format!("No matching repository with url {clone_url} in the configuration file.")
    })?;

    let hook_log_id = db::add_hook_log(&db.pool, hook)?.id;

    let hook_log_link = format!("{}/?id={}", state_config.base_url, hook_log_id);

    let webhook_task = webhook::Task {
        hook: hook.clone(),
        hook_log_id,
        hook_log_link: hook_log_link.clone(),
        db,
        mailer,
    };

    // Spawn and detach a task that runs the command and fills the output in the log.
    // This is useful to give a quick response to the git server in case that the command has a long
    // execution time. It prevents reaching the webhook timeout of the git server.
    task::spawn(async move { webhook_task.run().await });

    Ok(hook_log_link.into_response())
}
