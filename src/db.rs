use anyhow::{anyhow, Context, Result};
use chrono::offset::Local;
use diesel::{
    prelude::*,
    r2d2::{ConnectionManager, Pool, PooledConnection},
    sqlite::Sqlite,
};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use tracing::error;

use crate::{
    config::Hook,
    models::{HookLog, NewHookLog},
    schema::hooklog,
};

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations");

type DBConnectionManager = ConnectionManager<SqliteConnection>;
pub type DBPool = Pool<DBConnectionManager>;
type DBConnection = PooledConnection<DBConnectionManager>;

pub fn run_migrations(connection: &mut impl MigrationHarness<Sqlite>) -> Result<()> {
    match connection.run_pending_migrations(MIGRATIONS) {
        Err(e) => Err(anyhow!("{e:?}")),
        Ok(_) => Ok(()),
    }
}

pub fn establish_connection_pool(sqlite_db_path: &str) -> Result<DBPool> {
    let manager = DBConnectionManager::new(sqlite_db_path);

    Pool::builder()
        .build(manager)
        .context("Could not build database connection pool!")
}

pub fn get_conn(pool: &DBPool) -> Result<DBConnection> {
    pool.get().context("Could not get database pool!")
}

pub fn add_hook_log(pool: &DBPool, hook: &Hook) -> Result<HookLog> {
    let conn = &mut get_conn(pool)?;

    let command_with_args = hook.command.to_owned() + " " + &hook.args.join(" ");

    let new_hook_log = NewHookLog {
        datetime: &Local::now().format("%d.%m.%Y %T").to_string(),
        clone_url: &hook.clone_url,
        command_with_args: &command_with_args,
        current_dir: &hook.current_dir,
    };

    diesel::insert_into(hooklog::table)
        .values(&new_hook_log)
        .get_result::<HookLog>(conn)
        .context("Could not insert hook log!")
}

pub fn fill_hook_log(
    pool: &DBPool,
    hook_log_id: i32,
    stdout: &[u8],
    stderr: &[u8],
    status_code: Option<i32>,
) {
    let conn = &mut match get_conn(pool) {
        Ok(pool) => pool,
        Err(e) => {
            error!("Could not get a database pool connection to fill hook log with id {hook_log_id}: {e}");
            return;
        }
    };

    match diesel::update(hooklog::dsl::hooklog.find(hook_log_id))
        .set((
            hooklog::dsl::stdout.eq(Some(
                std::str::from_utf8(stdout).unwrap_or("Could not convert stdout to str!"),
            )),
            hooklog::dsl::stderr.eq(Some(
                std::str::from_utf8(stderr).unwrap_or("Could not convert stderr to str!"),
            )),
            hooklog::dsl::status_code.eq(status_code),
        ))
        .execute(conn)
    {
        Ok(_) => (),
        Err(e) => {
            error!("Could not update hook log with id {hook_log_id}: {e}");
        }
    };
}

pub fn get_hook_log(pool: &DBPool, id: i32) -> Result<HookLog> {
    // id=0 not allowed!

    let conn = &mut get_conn(pool)?;

    let hl = if id > 0 {
        hooklog::dsl::hooklog.find(id).first(conn)
    } else {
        hooklog::dsl::hooklog
            .order(hooklog::dsl::id.desc())
            .offset((-id - 1).into())
            .first(conn)
    };

    hl.context("Could not get hook log!")
}
