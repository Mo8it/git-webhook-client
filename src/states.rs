use anyhow::Result;
use axum::extract::FromRef;
use std::sync::Arc;

use crate::{config, db, mailer::Mailer};

pub struct DB {
    pub pool: db::DBPool,
}

impl DB {
    pub fn build(sqlite_db_path: &str) -> Result<Self> {
        Ok(Self {
            pool: db::establish_connection_pool(sqlite_db_path)?,
        })
    }
}

pub struct StateConfig {
    pub secret: Vec<u8>,
    pub base_url: String,
    pub hooks: Vec<config::Hook>,
}

impl From<config::Config> for StateConfig {
    fn from(config: config::Config) -> Self {
        Self {
            secret: config.secret.as_bytes().to_owned(),
            base_url: config.base_url,
            hooks: config.hooks,
        }
    }
}

impl StateConfig {
    pub fn get_hook(&self, clone_url: &str) -> Option<&config::Hook> {
        self.hooks.iter().find(|&hook| hook.clone_url == clone_url)
    }
}

#[derive(Clone, FromRef)]
pub struct AppState {
    pub config: Arc<StateConfig>,
    pub mailer: Arc<Mailer>,
    pub db: Arc<DB>,
}

impl AppState {
    pub fn build(config: config::Config, mailer: Mailer) -> Result<Self> {
        let sqlite_db_path = config.sqlite_db_path.clone();

        Ok(Self {
            config: Arc::new(StateConfig::from(config)),
            mailer: Arc::new(mailer),
            db: Arc::new(DB::build(&sqlite_db_path)?),
        })
    }
}

pub trait ConfigState {
    fn config(&self) -> Arc<StateConfig>;
}

impl ConfigState for AppState {
    fn config(&self) -> Arc<StateConfig> {
        self.config.clone()
    }
}
