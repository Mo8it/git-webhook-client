use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
};
use tracing::error;

pub struct AppError {
    inner: anyhow::Error,
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        error!("{:?}", self.inner);

        (StatusCode::BAD_REQUEST, format!("{:?}", self.inner)).into_response()
    }
}

impl From<anyhow::Error> for AppError {
    fn from(err: anyhow::Error) -> Self {
        Self { inner: err }
    }
}

impl From<&str> for AppError {
    fn from(s: &str) -> Self {
        Self {
            inner: anyhow::Error::msg(s.to_string()),
        }
    }
}
