use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::filter::LevelFilter;

use crate::config;

pub fn init_logger(logging_config: &config::Logging) -> WorkerGuard {
    let file_appender =
        tracing_appender::rolling::never(&logging_config.directory, &logging_config.filename);
    let (non_blocking, guard) = tracing_appender::non_blocking(file_appender);

    tracing_subscriber::fmt()
        .with_max_level(LevelFilter::INFO)
        .with_writer(non_blocking)
        .init();

    guard
}
