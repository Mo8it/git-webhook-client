// @generated automatically by Diesel CLI.

diesel::table! {
    hooklog (id) {
        id -> Integer,
        datetime -> Text,
        clone_url -> Text,
        command_with_args -> Text,
        current_dir -> Text,
        stdout -> Nullable<Text>,
        stderr -> Nullable<Text>,
        status_code -> Nullable<Integer>,
    }
}
