use anyhow::{Context, Result};
use lettre::{
    address::Address,
    message::{Mailbox, Message, MessageBuilder},
    transport::{
        smtp::{authentication::Credentials, AsyncSmtpTransport},
        AsyncTransport,
    },
    Tokio1Executor,
};
use std::mem;

use crate::config;

type ASmtpTransport = AsyncSmtpTransport<Tokio1Executor>;

pub struct Mailer {
    mailer: ASmtpTransport,
    message_builder: MessageBuilder,
}

impl Mailer {
    pub fn build(config: &mut config::Config) -> Result<Self> {
        let creds = Credentials::new(
            mem::take(&mut config.email_server.email),
            mem::take(&mut config.email_server.password),
        );

        let mailer = ASmtpTransport::relay(&config.email_server.server_name)
            .context("Failed to connect to the email server!")?
            .credentials(creds)
            .build();

        let message_builder = Message::builder()
            .from(Mailbox::new(
                Some(mem::take(&mut config.email_from.name)),
                Address::new(&config.email_from.user, &config.email_from.domain)
                    .context("Failed to create the From email address!")?,
            ))
            .to(Mailbox::new(
                Some(mem::take(&mut config.email_to.name)),
                Address::new(&config.email_to.user, &config.email_to.domain)
                    .context("Failed to create the To email address!")?,
            ));

        Ok(Self {
            mailer,
            message_builder,
        })
    }

    pub async fn send(&self, hook_name: &str, hook_log_link: &str, status: &str) -> Result<()> {
        let email = self
            .message_builder
            .clone()
            .subject(format!("GWC {}: {}", hook_name, status))
            .body(hook_log_link.to_string())
            .context("Failed to build email!")?;

        self.mailer
            .send(email)
            .await
            .context("Failed to send email!")?;

        Ok(())
    }
}
