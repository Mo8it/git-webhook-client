use std::sync::Arc;
use tokio::process::Command;
use tracing::{error, info};

use crate::{config, db, mailer::Mailer, states};

pub struct Task {
    pub hook: config::Hook,
    pub hook_log_id: i32,
    pub hook_log_link: String,
    pub db: Arc<states::DB>,
    pub mailer: Arc<Mailer>,
}

impl Task {
    pub async fn run(&self) {
        info!("Running webhook for repo: {}", self.hook.clone_url);

        let stdout: Vec<u8>;
        let stderr: Vec<u8>;
        let status_code: Option<i32>;

        match Command::new(&self.hook.command)
            .args(&self.hook.args)
            .current_dir(&self.hook.current_dir)
            .output()
            .await
        {
            Ok(output) => {
                stdout = output.stdout;
                stderr = output.stderr;
                status_code = output.status.code();
            }
            Err(e) => {
                stdout = Vec::new();
                stderr = format!("Error while running the hook command: {e}")
                    .as_bytes()
                    .to_vec();
                status_code = Some(1);
            }
        };

        let status = if status_code == Some(0) { "Ok" } else { "Err" };

        db::fill_hook_log(
            &self.db.pool,
            self.hook_log_id,
            &stdout,
            &stderr,
            status_code,
        );

        match self
            .mailer
            .send(&self.hook.name, &self.hook_log_link, status)
            .await
        {
            Ok(_) => info!("Sent email with hook name {}", self.hook.name),
            Err(e) => error!("{e:?}"),
        };
    }
}
