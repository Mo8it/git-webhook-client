use anyhow::{Context, Result};
use serde::Deserialize;
use std::{env, fs::File, io::BufReader};

#[derive(Deserialize)]
pub struct SocketAddress {
    pub address: [u8; 4],
    pub port: u16,
}

#[derive(Deserialize)]
pub struct EmailServer {
    pub server_name: String,
    pub email: String,
    pub password: String,
}

#[derive(Deserialize)]
pub struct Address {
    pub name: String,
    pub user: String,
    pub domain: String,
}

#[derive(Deserialize)]
pub struct Logging {
    pub directory: String,
    pub filename: String,
}

#[derive(Clone, Deserialize)]
pub struct Hook {
    pub name: String,
    pub clone_url: String,
    pub current_dir: String,
    pub command: String,
    pub args: Vec<String>,
}

#[derive(Deserialize)]
pub struct Config {
    pub secret: String,
    pub base_url: String,
    pub sqlite_db_path: String,
    pub socket_address: SocketAddress,
    pub email_server: EmailServer,
    pub email_from: Address,
    pub email_to: Address,
    pub logging: Logging,
    pub hooks: Vec<Hook>,
}

impl Config {
    pub fn build() -> Result<Self> {
        let config_file_var = "GWC_CONFIG_FILE";
        let config_path = env::var(config_file_var)
            .with_context(|| format!("Environment variable {config_file_var} missing!"))?;

        let config_file = File::open(&config_path)
            .with_context(|| format!("Can not open the config file at the path {config_path}"))?;
        let config_reader = BufReader::new(config_file);
        let config: Self = serde_yaml::from_reader(config_reader)
            .context("Can not parse the YAML config file!")?;

        Ok(config)
    }
}
