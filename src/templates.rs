use askama::Template;

use crate::models;

#[derive(Template)]
#[template(path = "hook_log.txt")]
pub struct HookLog {
    pub id: i32,
    pub datetime: String,
    pub clone_url: String,
    pub command_with_args: String,
    pub current_dir: String,
    pub stdout: String,
    pub stderr: String,
    pub status_code: i32,
}

impl From<models::HookLog> for HookLog {
    fn from(hook_log: models::HookLog) -> Self {
        Self {
            id: hook_log.id,
            datetime: hook_log.datetime,
            clone_url: hook_log.clone_url,
            command_with_args: hook_log.command_with_args,
            current_dir: hook_log.current_dir,
            stdout: hook_log.stdout.unwrap_or_default(),
            stderr: hook_log.stderr.unwrap_or_default(),
            status_code: hook_log.status_code.unwrap_or_default(),
        }
    }
}
