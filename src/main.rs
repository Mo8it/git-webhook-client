mod config;
mod db;
mod errors;
mod extractors;
mod logging;
mod mailer;
mod models;
mod routes;
mod schema;
mod states;
mod templates;
mod webhook;

use anyhow::Result;
use axum::{
    routing::{get, post},
    Router, Server,
};
use axum_extra::routing::SpaRouter;
use std::{
    net::{IpAddr, Ipv4Addr, SocketAddr},
    process,
};
use tracing::info;

async fn init() -> Result<()> {
    let mut config = config::Config::build()?;
    let mailer = mailer::Mailer::build(&mut config)?;

    let address = config.socket_address.address;
    let socket_address = SocketAddr::new(
        IpAddr::V4(Ipv4Addr::new(
            address[0], address[1], address[2], address[3],
        )),
        config.socket_address.port,
    );

    let _tracing_gurad = logging::init_logger(&config.logging);

    let app_state = states::AppState::build(config, mailer)?;

    db::run_migrations(&mut db::get_conn(&app_state.db.pool)?)?;

    let api_routes = Router::new().route("/trigger", post(routes::trigger));
    let routes = Router::new()
        .route("/", get(routes::index))
        .nest("/api", api_routes)
        .with_state(app_state);

    let spa = SpaRouter::new("/static", "static");

    let app = Router::new().merge(routes).merge(spa);

    info!("Starting server");
    Server::bind(&socket_address)
        .serve(app.into_make_service())
        .await
        .unwrap();

    Ok(())
}

#[tokio::main]
async fn main() {
    if let Err(e) = init().await {
        eprintln!("{e:?}");
        process::exit(1);
    };
}
