use anyhow::Context;
use axum::{
    async_trait,
    body::Bytes,
    extract::{FromRequest, FromRequestParts},
    http::request::{Parts, Request},
};
use hmac::{Hmac, Mac};
use serde_json::Value;
use sha2::Sha256;

use crate::{errors::AppError, states};

pub struct ReceivedSignature(pub Vec<u8>);

#[async_trait]
impl<S> FromRequestParts<S> for ReceivedSignature
where
    S: Send + Sync,
{
    type Rejection = AppError;

    async fn from_request_parts(parts: &mut Parts, _: &S) -> Result<Self, Self::Rejection> {
        let mut received_signatures = parts.headers.get_all("X-GITEA-SIGNATURE").iter();

        let received_signature = received_signatures.next().context("Missing signature!")?;

        if received_signatures.next().is_some() {
            return Err("Received more than one signature!".into());
        }

        let received_signature = hex::decode(received_signature)
            .context("Can not hex decode the received signature!")?;

        Ok(ReceivedSignature(received_signature.to_vec()))
    }
}

impl ReceivedSignature {
    pub fn is_valid(&self, secret: &[u8], body: &[u8]) -> bool {
        let mut mac = Hmac::<Sha256>::new_from_slice(secret)
            .expect("Can not generate a mac from the secret!");
        mac.update(body);
        let expected_signature = mac.finalize().into_bytes();

        self.0[..] == expected_signature[..]
    }
}

pub struct ValidatedBody(pub Bytes);

#[async_trait]
impl<S, B> FromRequest<S, B> for ValidatedBody
where
    Bytes: FromRequest<S, B>,
    B: Send + 'static,
    S: Send + Sync + states::ConfigState,
{
    type Rejection = AppError;

    async fn from_request(req: Request<B>, state: &S) -> Result<Self, Self::Rejection> {
        let (mut parts, body) = req.into_parts();
        let received_signature = ReceivedSignature::from_request_parts(&mut parts, state).await?;
        let req = Request::from_parts(parts, body);

        let body = Bytes::from_request(req, state)
            .await
            .map_err(|_| "Can not extract body as Bytes!")?;

        let state_config = state.config();

        if !received_signature.is_valid(&state_config.secret, &body) {
            return Err("Invalid signature!".into());
        }

        Ok(Self(body))
    }
}

pub struct ValidatedJson(pub Value);

#[async_trait]
impl<S, B> FromRequest<S, B> for ValidatedJson
where
    Bytes: FromRequest<S, B>,
    B: Send + 'static,
    S: Send + Sync + states::ConfigState,
{
    type Rejection = AppError;

    async fn from_request(req: Request<B>, state: &S) -> Result<Self, Self::Rejection> {
        let body = ValidatedBody::from_request(req, state).await?.0;

        let json: Value =
            serde_json::from_slice(&body).context("Can not parse the request body into JSON!")?;

        Ok(Self(json))
    }
}
