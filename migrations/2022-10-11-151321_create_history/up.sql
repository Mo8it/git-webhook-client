CREATE TABLE hooklog (
    id INTEGER NOT NULL PRIMARY KEY,
    datetime TEXT NOT NULL,
    clone_url TEXT NOT NULL,
    command_with_args TEXT NOT NULL,
    current_dir TEXT NOT NULL,
    stdout TEXT,
    stderr TEXT,
    status_code INTEGER CHECK (status_code >= 0)
);
