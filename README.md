# Git Webhook Client

Git webhook client that runs commands after a webhook event and shows their output.

Currently, only Gitea is supported. If you want support for Gitlab or Github, then please open an issue.

## Features

- Verify the webhook event with a secret.
- Run a configured command to a specific repository on a webhook event.
- Save the output of the command.
- Show an output by visiting its URL.
- Send email after a webhook trigger.
- Support configuration for multiple repositories.
- Written in Rust :D

## Getting started

First, clone the repository and create the configuration file (see below).

### Configuration

The program looks for the configuration file set with the environment variable `GWC_CONFIG_FILE`.

Use the commented template configuration file `template_config.yaml` as a starting point.

### Running

#### Containerized

With Docker or Podman, use the compose file `compose.yaml` as a starting point.

#### Not containerized

Requirements:

- `cargo` to compile the source code.
- Development package for SQLite (`sqlite-devel` on Fedora, `libsqlite3-dev` on Debian)

Run the command `cargo install --path .` to compile.

Run the command `GWC_CONFIG_FILE=/path/to/config.yaml ~/.cargo/bin/git-webhook-client` to start the client.

#### Setup on the git server

Setup the webhook for the configured repositories on the git server. Don't forget to enter the same secret that you did specify in the YAML configuration file.

#### Show output

After an event, the client responds with a URL that shows the log. The id in that URL is important and specific to this event.

If you want to see the last log, just visit the `base_url` from the configuration.

To see a specific log with an id, visit the URL: `base_url/?id=THE_ID_OF_THE_EVENT`.

You can specify a negative ID to see the last events. `id=-1` corresponds to the last log, `id=-2` corresponds to the log before it and so on.

## Note

This is my first Rust project and I am still learning. If you have any suggestions, just open an issue!
